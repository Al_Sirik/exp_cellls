function randomColor() {
    function pad(width, string, padding) {
        return (width <= string.length) ? string : pad(width, padding + string, padding)
    }
    var res = '#' + pad(6, Math.floor(Math.random() * 16777215).toString(16), '0');
    return res;
}

function randomImage() {
    return 'http://lorempixel.com/300/300/?' + Math.random();
}


$(function() {
    var allElements = $('.coll');
    $(allElements).each(function(index) {
        $(this).find('.background').css('background-image', 'url(' + randomImage() + ')');
    });
    var clips;
    function rebuild() {
        allElements = $('.coll');
        var contwidth = $('.block').width();
        var itemwidth = contwidth / 5;
        var count = Math.floor(contwidth / itemwidth);
        var width = contwidth / count;
        allElements.width(width);
        allElements.height(width);
        $('.block').height(Math.ceil(allElements.length / count) * width);

        $(allElements).each(function(index) {
            var $this = $(this);
            $this.data('index', index);
            $this.data('neib', getNeibour(index, count));
        });
        clips = getClips(width);
        setStyle(allElements, 'default');
    }

    $(window).on('resize', function() {
        rebuild();
    }).resize();



    function setStyle(element, type) {
        if (element) {
            var clip = clips[type];
            $(element).attr('data-orient', type).find('.background').css('-webkit-clip-path', 'polygon(' + clip + ")");
        }
    }

    function getNeibour(index, inrowCount) {
        function getInRow(index) {
            return  index % inrowCount;
        }
        function getLeft(index) {
            if (getInRow(index) > 0) {
                return index - 1;
            } else {
                return 'notexist';
            }
        }
        function getRight(index) {
            if (index >= 0 && getInRow(index) < inrowCount - 1) {
                return index + 1;
            } else {
                return 'notexist';
            }
        }
        var top = index - inrowCount;
        var bottom = index + inrowCount;

        var res = {
            _inrow: getInRow(index),
            top: top,
            topleft: getLeft(top),
            topright: getRight(top),
            bottom: bottom,
            bottomleft: getLeft(bottom),
            bottomright: getRight(bottom),
            left: getLeft(index),
            right: getRight(index),
        };
        return res;
    }

    $('.coll').hover(function() {
        var $element = $(this);
        var neib = $element.data('neib');
        setStyle($element, 'active');
        $.each(neib, function(k, element) {
            setStyle(allElements[element], k);
        });

    }, function() {
        var $element = $(this);
        var neib = $element.data('neib');
        setStyle($element, 'default');
        $.each(neib, function(k, element) {
            setStyle(allElements[element], 'default');
        });
    });
});







function getClips(width) {

    function getClip(type) {
        var delta = width / 10;
        var height = width;
        var clip = {
            tl: [delta, delta],
            tr: [width + delta, delta],
            br: [width + delta, height + delta],
            bl: [delta, height + delta],
        };

        if (type === 'left') {
            clip.tr = [width, 0];
            clip.br = [width, height + delta * 2];
        } else if (type === 'right') {
            clip.tl = [delta * 2, 0];
            clip.bl = [delta * 2, height + delta * 2];
        } else if (type === 'top') {
            clip.bl = [0, height];
            clip.br = [width + delta * 2, height];
        } else if (type === 'bottom') {
            clip.tl = [0, delta * 2];
            clip.tr = [width + delta * 2, delta * 2];
        } else if (type === 'topright') {
            clip.bl = [delta * 2, height];
        } else if (type === 'topleft') {
            clip.br = [width, height];
        } else if (type === 'bottomright') {
            clip.tl = [delta * 2, delta * 2];
        } else if (type === 'bottomleft') {
            clip.tr = [width, delta * 2];
        } else if (type === 'active') {
            clip.tl = [0, 0]
            clip.tr = [width + delta * 2, 0];
            clip.br = [width + delta * 2, height + delta * 2];
            clip.bl = [0, height + delta * 2];
        }
        return clip.tl[0] + 'px ' + clip.tl[1] + 'px, '
                + clip.tr[0] + 'px ' + clip.tr[1] + 'px, '
                + clip.br[0] + 'px ' + clip.br[1] + 'px, '
                + clip.bl[0] + 'px ' + clip.bl[1] + 'px'

    }

    var clips = {};
    var types = [
        'topleft', 'top', 'topright',
        'left', 'active', 'right',
        'bottomleft', 'bottom', 'bottomright',
        'default'
    ];
    for (var i = 0; i < types.length; i++) {
        clips[types[i]] = getClip(types[i]);
    }
    return clips;
}
